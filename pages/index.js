import { useEffect } from "react";
import Gallery from "../src/components/Gallery";
import Layout from "../src/components/Layout";
import UpNext from "../src/components/UpNext";
import { useMovie } from "../src/context/movieContext";
import request from "../src/utility/request";
function Home({ topMovies }) {
  const { setMovies, setPage } = useMovie();

  useEffect(() => {
    setMovies(topMovies);
    setPage(1);
  });
  return (
    <Layout>
      <UpNext />
      <Gallery />
    </Layout>
  );
}

export async function getStaticProps(context) {
  let movies;
  let errors = null;
  try {
    movies = await request(
      "https://api.themoviedb.org/3/trending/all/day",
      "GET"
    );
  } catch (err) {
    if (err.status !== 404) {
      errors = err;
    }
  }

  return {
    props: {
      topMovies: movies ? movies : {},
      errors: errors,
    },
    revalidate: 2,
  };
}

export default Home;
