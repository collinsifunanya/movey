import { useEffect } from "react";
import Gallery from "../../src/components/Gallery";
import Layout from "../../src/components/Layout";
import { useMovie } from "../../src/context/movieContext";
import request from "../../src/utility/request";

function Page({ topMovies, page }) {
  const { setMovies, setPage } = useMovie();

  useEffect(() => {
    setMovies(topMovies);
    setPage(page);
  });
  return (
    <Layout>
      <Gallery />
    </Layout>
  );
}

export async function getStaticPaths() {
  /**
        Because TMBD don't allow devs to specify the limit of the data returned from the API call
        I have to statically generate 25 pages with each page containing 20 movies per page
    
        Note that fallback is false. why? because I was only required to display the top 500 movies
        moving to page 26 add additional 20 movies making it 520 movies in total
*/

  const paths = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 10, 11, 12, 13,
    14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
  ].map((page) => ({
    params: {
      page: String(page),
    },
  }));

  return {
    fallback: false,
    paths,
  };
}

export async function getStaticProps(context) {
  const page = context.params.page;
  let movies;
  let errors;
  try {
    movies = await request(
      `https://api.themoviedb.org/3/trending/all/day?page=${page}`,
      "GET"
    );
  } catch (err) {
    if (err.status !== 404) {
      errors = err;
    }
  }

  return {
    props: {
      topMovies: movies ? movies : "",
      page: page,
    },
    revalidate: 2,
  };
}

export default Page;
