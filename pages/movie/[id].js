import { useRouter } from "next/router";
import day from "dayjs";
import Banner from "../../src/components/UpNext/Banner";
import Layout from "../../src/components/Layout";
import request from "../../src/utility/request";
import { Rate } from "../../src/components/icons";
import { useSession } from "../../src/context/sessionContext";

function Movie({ movie }) {
  const { back } = useRouter();
  const { toggleAddRateToSession, session } = useSession();

  return (
    <Layout>
      <button
        className="border border-yellow-200 h-8 w-20 rounded mt-3"
        onClick={() => back()}
      >
        Back
      </button>
      <div className="my-10">
        <Banner
          tagline={movie?.tagline}
          poster={movie?.poster_path}
          title={movie?.original_title || movie?.original_name}
        />
      </div>

      <div className="mt-10 flex space-x-3">
        <Rate
          active={session.includes(movie?.id).toString()}
          onClick={() => toggleAddRateToSession(movie?.id)}
        />
        <span>{movie?.vote_average}</span>
      </div>
      <span className="text-yellow-200 text-xs my-2 block">
        {day(movie?.release_date).format("MMMM D, YYYY")}
      </span>

      <p className="text-lg">{movie?.overview}</p>
    </Layout>
  );
}

export async function getStaticPaths() {
  /**
        Because TMBD don't allow devs to specify the limit of the data returned from the API call
        I requested the first 20 to build the first base of the statically generated pages.  
    
        Note that fallback is true. why? because the first two ids of the  the trending ids is known at build time. 
        we can generate the rest in production.

*/
  let first20;
  try {
    first20 = await request(
      `https://api.themoviedb.org/3/trending/all/day`,
      "GET"
    );
  } catch (error) {
    first20 = [];
  }

  const paths = first20?.results?.map((movie) => ({
    params: {
      id: String(movie.id),
    },
  }));

  return {
    fallback: true,
    paths: paths,
  };
}

export async function getStaticProps(context) {
  const id = context.params.id;
  let movie;
  let errors;
  try {
    movie = await request(
      `https://api.themoviedb.org/3/movie/${id}
      `,
      "GET"
    );
  } catch (err) {
    if (err.status !== 404) {
      errors = err;
    }
  }

  return {
    props: {
      movie: movie ? movie : "",
    },
    revalidate: 2,
  };
}

export default Movie;
