import dynamic from "next/dynamic";
import { MovieProvider } from "../src/context/movieContext";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../src/styles/global.css";

const SessionProvider = dynamic(
  () =>
    import("../src/context/sessionContext").then((mod) => mod.SessionProvider),
  { ssr: false }
);

function MyApp({ Component, pageProps }) {
  return (
    <MovieProvider>
      <SessionProvider>
        <Component {...pageProps} />
      </SessionProvider>
    </MovieProvider>
  );
}

export default MyApp;
