function sortMovie(movie = [], sorter) {
  return movie.sort(function (a, b) {
    var nameA = a.title ? a.title[0].toLowerCase() : "z";
    var nameB = b.title ? b.title[0].toLowerCase() : "z";
    if (sorter === "asc") {
      if (nameA < nameB) {
        return -1;
      }

      if (nameA > nameB) {
        return 1;
      }

      // names must be equal
      return 0;
    }

    if (sorter === "dsc") {
      if (nameA > nameB) {
        return -1;
      }

      if (nameA > nameB) {
        return 1;
      }

      // names must be equal
      return 0;
    }
  });
}

export default sortMovie;
