const token = process.env.NEXT_PUBLIC_MDB_AUTH;

const request = async (url, method, data) => {
  const response = await fetch(url, {
    method: method, // *GET, POST, PUT, DELETE, etc.
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },

    body: JSON.stringify(data),
  });
  return response.json();
};

export default request;
