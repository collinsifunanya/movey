import Search from "./Search";

function Input({ className, ...props }) {
  const newClassName = [className].filter(Boolean).join(" ");
  return <input className={newClassName} {...props} />;
}

Input.Search = Search;

export default Input;
