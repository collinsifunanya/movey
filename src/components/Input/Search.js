import { Search as SearchIcon } from "../icons";
function Search({ className, containerStyle, ...props }) {
  const newClassName = [
    "w-full",
    "px-10",
    "border border-gray-200 focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent rounded-full",
    "transition duration-500 ease-in-out",
    className,
  ]
    .filter(Boolean)
    .join(" ");
  return (
    <div
      className="flex bg-white items-center relative w-full rounded-full"
      style={containerStyle}
    >
      <div className="absolute left-3">
        <SearchIcon />
      </div>

      <input className={newClassName} {...props} />
    </div>
  );
}

export default Search;
