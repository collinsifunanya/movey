import React from "react";
import { posterPrefix } from "../../utility/constants";
import Styles from "./upnext.module.css";
function Banner({ poster, title, tagline }) {
  const posterUrl = `${posterPrefix}${poster}`;
  return (
    <div
      className={`${Styles.movieBox} bg-no-repeat bg-cover bg-center  relative`}
      style={{
        backgroundImage: `url(${posterUrl})`,
      }}
    >
      <div
        className="absolute inset-x-0 bottom-0 lg:h-20 h-10 flex flex-col justify-center px-5"
        style={{ backgroundColor: "#000000d9" }}
      >
        <h1 className="text-white lg:text-3xl text-lg">{title}</h1>
        <span className="block text-sm text-gray-400">{tagline}</span>
      </div>
    </div>
  );
}

export default Banner;
