import Image from "next/image";
import { posterPrefix } from "../../utility/constants";

function NextMovieBox({ movies = [] }) {
  return (
    <div>
      {movies.map((movie) => (
        <div className="flex mb-3" key={movie.id}>
          <Image
            src={`${posterPrefix}${movie.poster_path}`}
            width={80}
            height={100}
          />
          <div className="mx-3 my-3">
            <h2 className="text-xl">
              {movie.original_title || movie.original_name}
            </h2>
          </div>
        </div>
      ))}
    </div>
  );
}

export default NextMovieBox;
