import Slider from "react-slick";
import { useMovie } from "../../context/movieContext";

import Banner from "./Banner";
import NextMovieBox from "./NextMovieBox";

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  autoplay: true,
};

function UpNext(props) {
  const { movies } = useMovie();

  return (
    <div className="lg:my-24 my-5 grid lg:grid-cols-3 grid-cols-1 gap-5">
      <div className="col-span-2">
        <Slider {...settings}>
          {movies?.results?.slice(2, 6).map((movie) => (
            <div key={movie.id}>
              <Banner
                poster={movie.poster_path}
                title={movie.original_title || movie.original_name}
              />
            </div>
          ))}
        </Slider>
      </div>
      <div>
        <p className="text-yellow-200 font-bold mb-2">Up Next</p>
        <NextMovieBox movies={movies?.results?.slice(2, 6)} />
      </div>
    </div>
  );
}

export default UpNext;
