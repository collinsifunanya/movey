import Link from "next/link";

function Logo(props) {
  return (
    <Link href="/">
      <p className={`md:text-2xl text-xl text-white cursor-pointer`}>
        M<span className="md:text-5xl text-3xl text-red-600">o</span>vey
      </p>
    </Link>
  );
}

export default Logo;
