import { useMovie } from "../../context/movieContext";
import { ArrowDown, ArrowUp } from "../icons";

function SorterICon(props) {
  const { sorter, setSorter } = useMovie();
  return (
    <div className="flex flex-col">
      <ArrowUp
        className="cursor-pointer"
        current={sorter}
        onClick={() => setSorter("asc")}
      />
      <ArrowDown
        className="cursor-pointer"
        current={sorter}
        onClick={() => setSorter("dsc")}
      />
    </div>
  );
}

export default SorterICon;
