import Link from "next/link";
import Image from "next/image";
import day from "dayjs";
import { useSession } from "../../context/sessionContext";
import { posterPrefix } from "../../utility/constants";
import { Rate } from "../icons";

function MovieCard({ poster, title, id, vote, date }) {
  const posterUrl = `${posterPrefix}${poster}`;

  const { toggleAddRateToSession, session } = useSession();

  return (
    <div
      className={`cursor-pointer ${
        session.includes(id) ? "border border-yellow-200 rounded shadow" : ""
      }`}
    >
      <div className="relative" style={{ paddingBottom: "80%" }}>
        <Image
          layout="fill"
          className="absolute h-full w-full object-cover"
          alt={title}
          src={posterUrl}
        />
      </div>
      <div className="p-3 py-5">
        <div className="flex items-center space-x-3">
          <Rate
            active={session.includes(id).toString()}
            onClick={() => toggleAddRateToSession(id)}
          />
          <span>{vote}</span>
        </div>

        <div>
          <span className="text-yellow-200 text-xs mt-5 block">
            {day(date).format("MMMM D, YYYY")}
          </span>
          <Link href={`/movie/${id}`}>
            <p className="lg:text-lg text-sm my-1">{title}</p>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default MovieCard;
