import React, { useMemo } from "react";
import { useRouter } from "next/router";
import { useMovie } from "../../context/movieContext";
import sortMovie from "../../utility/sort";
import SorterICon from "../Sorter";
import MovieCard from "./MovieCard";

function Gallery(props) {
  const { push, back } = useRouter();
  const { movies, sorter, page } = useMovie();
  const memoizedMovie = useMemo(
    () => sortMovie(movies?.results, sorter),
    [sorter, movies]
  );

  return (
    <div className="py-8">
      <div className="flex justify-between my-3 items-center relative">
        <p className="text-white font-bold mb-2 headings">
          {`This week's top movies`}
        </p>
        <SorterICon />
      </div>
      <div className="border-0 border-t border-opacity-5 border-gray-200 my-2 grid lg:grid-cols-4 grid-cols-2 gap-5 gap-y-10">
        {memoizedMovie?.map((movie) => (
          <MovieCard
            key={movie.id}
            date={movie.first_air_date}
            id={movie.id}
            vote={movie.vote_average}
            poster={movie.poster_path}
            title={movie.title || movie.original_name}
          />
        ))}
      </div>

      <div className="flex justify-end space-x-3">
        {page > 1 && (
          <button
            className="border border-yellow-200 h-8 w-20 rounded"
            onClick={() => back()}
          >
            Prev
          </button>
        )}
        {page < 25 && (
          <button
            className="border border-yellow-200 h-8 w-20 rounded"
            onClick={() => push(`/page/${Number(page) + 1}`)}
          >
            Next
          </button>
        )}
      </div>
    </div>
  );
}

export default Gallery;
