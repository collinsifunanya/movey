import Navbar from "./Navbar";

function Wrapper({ children }) {
  return (
    <div className="pb-10">
      <main>
        <Navbar />
        <div className="lg:px-32 px-3">{children}</div>
      </main>
    </div>
  );
}

export default Wrapper;
