import Avatar from "../Avatar/";
import { Menu } from "../icons";
import Input from "../Input";
import Logo from "../Logo";

function Navbar({ setToggleSidebar }) {
  return (
    <nav
      className="h-24 flex items-center lg:px-32 px-3"
      style={{ backgroundColor: "#131313" }}
    >
      <div className="grid grid-cols-4 w-full">
        {/* Contains Logo */}
        <div>
          <Logo />
        </div>

        {/* Contains search bar and hidden on mobile */}
        <div className="col-span-2  lg:flex items-center px-3 hidden">
          <Input.Search placeholder="search movie" className="h-10" />
        </div>
        {/* Contains username, avatar and hidden on mobile */}
        <div className="lg:flex items-center space-x-5 hidden justify-end">
          <span className="text-white">Collins Ogbuzuru</span>
          <Avatar />
        </div>

        {/* Contains the toggle icon and is only visible on mobile */}
        <div className="col-span-3  flex items-center justify-end  lg:hidden">
          <Menu />
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
