import React from "react";

function Avatar(props) {
  return <div className="h-10 w-10 rounded-full bg-gray-300"></div>;
}

export default Avatar;
