import { createContext, useContext, useEffect, useState } from "react";
const AppContext = createContext();

function SessionProvider({ children }) {
  const initSession = JSON.parse(localStorage.getItem("movieSession")) || [];
  const [session, setSession] = useState(initSession);
  const [status, setStatus] = useState(-1);

  const toggleAddRateToSession = (id) =>
    setSession((prev) =>
      prev.indexOf(id) == -1
        ? [...prev, id]
        : prev.filter((prevId) => prevId !== id)
    );

  const value = {
    toggleAddRateToSession,
    session,
  };

  useEffect(() => {
    /**
     * On Page load I check of app status
     * If status is -1 , the app is just loading for the first time
     * set status to 1 to indicate that app is loaded at this point
     * else
     * set items to storage
     */
    status === 1
      ? localStorage.setItem("movieSession", JSON.stringify(session))
      : setStatus(1);
  }, [session]);

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
}

const useSession = () => {
  const context = useContext(AppContext);

  if (context === undefined) {
    throw new Error("useSession must be used within an SessionProvider");
  }

  return context;
};

export { SessionProvider, useSession };
