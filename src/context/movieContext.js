import { createContext, useContext, useState } from "react";
const AppContext = createContext();

function MovieProvider({ children }) {
  const [movies, setMovies] = useState({});
  const [sorter, setSorter] = useState("asc");
  const [page, setPage] = useState(1);
  const value = {
    movies,
    setMovies,
    sorter,
    setSorter,
    page,
    setPage,
  };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
}

const useMovie = () => {
  const context = useContext(AppContext);

  if (context === undefined) {
    throw new Error("useMovie must be used within an MovieProvider");
  }

  return context;
};

export { MovieProvider, useMovie };
